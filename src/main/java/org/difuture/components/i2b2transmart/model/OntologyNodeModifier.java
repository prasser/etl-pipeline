/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.model;

/**
 * Modifier node
 * 
 * @author Helmut Spengler
 * @author Fabian Prasser
 * @author Claudia Lang
 */
public class OntologyNodeModifier extends OntologyNodeConcept {

    /** Applied concept */
    private final String targetPath;

    /**
     * Creates a new instance
     * 
     * @param prefix
     * @param parent
     * @param name
     * @param numeric
     * @param target
     */
    public OntologyNodeModifier(String prefix,
                                OntologyNodeConcept parent,
                                String name,
                                boolean numeric,
                                OntologyNodeConcept target) {
        super(prefix, parent, name, numeric, false);
        this.targetPath = target.getPath();
    }

    /**
     * Internal constructor for deserialization
     * 
     * @param parent
     * @param name
     * @param code
     * @param numeric
     * @param targetPath
     */
    protected OntologyNodeModifier(OntologyNodeConcept parent,
                                   String name,
                                   String code,
                                   boolean numeric,
                                   String targetPath) {
        super(parent, name, code, numeric, false);
        this.targetPath = targetPath;
    }

    /**
     * Return the depth For modifiers it should be the same as the one for the
     * parent. When an new modifier is created, its depth is incremented
     * automtically, so here it is decremented
     * 
     * @return
     */
    public int getDepth() {
        return super.getDepth() - 1;
    }

    /**
     * Returns the full name. For concepts this is the path and for modifiers it
     * is the postfix of the path
     * 
     * @return
     */
    @Override
    public String getFullName() {
        String path = super.getPath();
        return "\\" + path.replace(targetPath, "");
    }

    /**
     * Return target
     * 
     * @return
     */
    public String getTargetPath() {
        return targetPath;
    }

    /**
     * Modifiers are hidden
     * 
     * @return the DB code for the visual representation of this node
     */
    public String getVisualAttributes() {
        if (isLeaf()) { return "RA"; }
        // NOTE: Modifiers may be hierarchic. Right now they are flat.
        // Thats why folders for modifier are empty and are hidden
        return "DA";
    }

}
