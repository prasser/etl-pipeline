/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.configuration;

import java.io.File;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;

/**
 * The class Subject. Each subject represents a patient and her or his
 * demographic data.
 * 
 * @author Fabian Prasser
 * @author Helmut Spengler
 */
public class ConfigurationSubject {

    /** Whether to keep subjects with no visits */
    public final boolean          keepAll;

    /** File */
    public final File             file;
    /** Key - Subject id */
    public final ConfigurationKey key;
    /** Date format */
    public final String           dateFormat;
    /** Path (folder name of the patient attributes - default: Demographics) */
    public final String           path;
    /** Formatter for the format */
    private DateTimeFormatter     dateFormatter = null;
    /** Patient attribute */
    public String                 sex;
    /** Patient attribute */
    public String                 zip;
    /** Patient attribute */
    public String                 age;
    /** Patient attribute */
    public String                 birthYear;
    /** Patient attribute */
    public String                 birthMonth;
    /** Patient attribute */
    public String                 birthDate;
    /** Patient attribute */
    public String                 sexFemale;
    /** Patient attribute */
    public String                 sexMale;
    /** Patient attribute */
    public String                 sexUnknown;

    /** Patient attribute (i2b2 specific) */
    public String                 vitalStatus;
    /** Patient attribute (i2b2 specific) */
    public String                 language;
    /** Patient attribute (i2b2 specific) */
    public String                 race;
    /** Patient attribute (i2b2 specific) */
    public String                 maritalStatus;
    /** Patient attribute (i2b2 specific) */
    public String                 religion;
    /** Patient attribute (i2b2 specific) */
    public String                 stateCityZipPath;
    /** Patient attribute (i2b2 specific) */
    public String                 income;
    /** Patient attribute (i2b2 specific) */
    public String                 blob;

    /** Whether IDs should be loaded */
    public boolean                loadIDs;

    /**
     * Creates a new instance
     * 
     * @param file
     * @param key
     * @param dateFormat
     * @param keepAll
     * @param sex
     * @param zip
     * @param age
     * @param birthYear
     * @param birthMonth
     * @param birthDate
     * @param sexFemale
     * @param sexMale
     * @param sexUnknown
     * @param vitalStatus
     * @param language
     * @param race
     * @param maritalStatus
     * @param religion
     * @param statecityzipPath
     * @param income
     * @param blob
     * @param path
     * @param loadIDs
     */
    protected ConfigurationSubject(File file,
                                   ConfigurationKey key,
                                   String dateFormat,
                                   boolean keepAll,
                                   String sex,
                                   String zip,
                                   String age,
                                   String birthYear,
                                   String birthMonth,
                                   String birthDate,
                                   String sexFemale,
                                   String sexMale,
                                   String sexUnknown,
                                   String vitalStatus,
                                   String language,
                                   String race,
                                   String maritalStatus,
                                   String religion,
                                   String statecityzipPath,
                                   String income,
                                   String blob,
                                   String path,
                                   boolean loadIDs) {
        this.file = file;
        this.key = key;
        this.dateFormat = dateFormat;
        this.keepAll = keepAll;
        this.sex = sex;
        this.zip = zip;
        this.age = age;
        this.birthYear = birthYear;
        this.birthMonth = birthMonth;
        this.birthDate = birthDate;
        this.sexFemale = sexFemale;
        this.sexMale = sexMale;
        this.sexUnknown = sexUnknown;
        this.vitalStatus = vitalStatus;
        this.language = language;
        this.race = race;
        this.maritalStatus = maritalStatus;
        this.religion = religion;
        this.stateCityZipPath = statecityzipPath;
        this.income = income;
        this.blob = blob;
        this.path = path;
        this.loadIDs = loadIDs;
    }

    /**
     * Returns a formatter for the date
     * 
     * @return
     */
    public DateTimeFormatter getDateFormatter() {
        if (dateFormat == null) {
            return null;
        } else {
            if (dateFormatter == null) {
                dateFormatter = DateTimeFormatter.ofPattern(dateFormat);
            }
            return dateFormatter;
        }
    }

    /**
     * Gets the name of the attributes to read from the input file for the
     * subjects.
     * 
     * @return a set
     */
    public Set<String> getSelectedColumns() {
        Set<String> result = new HashSet<>();
        result.addAll(key.getKeyAttributes());
        if (sex != null) {
            result.add(sex);
        }
        if (age != null) {
            result.add(age);
        }
        if (zip != null) {
            result.add(zip);
        }
        if (birthYear != null) {
            result.add(birthYear);
        }
        if (birthMonth != null) {
            result.add(birthMonth);
        }
        if (birthDate != null) {
            result.add(birthDate);
        }
        if (sexFemale != null) {
            result.add(sexFemale);
        }
        if (sexMale != null) {
            result.add(sexMale);
        }
        if (sexUnknown != null) {
            result.add(sexUnknown);
        }
        if (vitalStatus != null) {
            result.add(vitalStatus);
        }
        if (language != null) {
            result.add(language);
        }
        if (race != null) {
            result.add(race);
        }
        if (maritalStatus != null) {
            result.add(maritalStatus);
        }
        if (religion != null) {
            result.add(religion);
        }
        if (stateCityZipPath != null) {
            result.add(stateCityZipPath);
        }
        if (income != null) {
            result.add(income);
        }
        if (blob != null) {
            result.add(blob);
        }
        return result;
    }
}
