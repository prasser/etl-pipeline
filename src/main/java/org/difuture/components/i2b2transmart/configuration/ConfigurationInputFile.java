/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.configuration;

import java.io.File;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The class ConfigurationInputFile. Represents the configuration of the input
 * file as configured in configuration.properties.
 * 
 * @author Fabian Prasser
 * @author Claudia Lang
 */
public class ConfigurationInputFile implements Cloneable {

    /** Input File */
    public final Set<File>                              file;
    /** Name */
    private String                                      name;
    /** Key - Either a subject key or an encounter key */
    private ConfigurationKey                            key;
    /** Whitelist or blacklist */
    private Set<String>                                 blacklist;
    /** Whitelist or blacklist */
    private Set<String>                                 whitelist;
    /** The mapping. */
    private Map<String, String>                         mappingColumns;
    /** Has encounter */
    private boolean                                     hasEncounter;
    /** Map to visit by timestamp */
    private boolean                                     isFindVisitViaTimestamp;
    /** Map to visit by timestamp */
    private boolean                                     isFindVisitViaTimestampOneToMany;
    /** Filter */
    private ConfigurationInputFileColumnFilter[]        columnFilter;
    /** EAV */
    private ConfigurationInputFileEAV                   eav;
    /** Number converter */
    private ConfigurationInputFileNumberFormat          numberConverter;
    /** Timestamp/ */
    public final String                                 startDay;
    /** Timestamp/ */
    public final String                                 startMonth;
    /** Timestamp/ */
    public final String                                 startYear;
    /** Timestamp/ */
    public final String                                 endDay;
    /** Timestamp/ */
    public final String                                 endMonth;
    /** Timestamp/ */
    public final String                                 endYear;
    /** Start */
    public final String                                 start;
    /** End */
    public final String                                 end;
    /** Use modifier as measure timestamp reliability - i2b2 specific */
    private Boolean                                     useMeasureTimestampReliability;
    /** Date format */
    public final String                                 dateFormat;
    /** Concept */
    public final String                                 concept;
    /** Hierarchical path */
    public final String                                 path;
    /** Charset */
    public final Charset                                charset;
    /** Value mappings */
    public final Map<String, ConfigurationValueMapping> valueMappings;
    /** Tmdl use timestamp - tMDataLoader-specific */
    private boolean                                     tmdlUseTimestamp;
    /** No summary */
    public final boolean                                noSummary;

    /**
     * Clone constructor
     * 
     * @param file
     * @param key
     * @param name
     * @param nonDuplicates
     * @param mappingColumns
     * @param whitelist
     * @param blacklist
     * @param hasEncounter
     * @param start
     * @param end
     * @param dateFormat
     * @param concept
     * @param numberConverter
     * @param path
     * @param startDay
     * @param startMonth
     * @param startYear
     * @param endDay
     * @param endMonth
     * @param endYear
     * @param isFindVisitViaTimestamp
     * @param isFindVisitViaTimestampOneToMany
     * @param columnFilter
     * @param eav
     * @param useMeasureTimestampReliability
     * @param noSummary
     */
    private ConfigurationInputFile(Set<File> file,
                                   ConfigurationKey key,
                                   String name,
                                   Map<String, String> mappingColumns,
                                   Set<String> whitelist,
                                   Set<String> blacklist,
                                   boolean hasEncounter,
                                   String start,
                                   String end,
                                   String dateFormat,
                                   String concept,
                                   ConfigurationInputFileNumberFormat numberConverter,
                                   String path,
                                   String startDay,
                                   String startMonth,
                                   String startYear,
                                   String endDay,
                                   String endMonth,
                                   String endYear,
                                   boolean isFindVisitViaTimestamp,
                                   boolean isFindVisitViaTimestampOneToMany,
                                   Charset charset,
                                   ConfigurationInputFileColumnFilter[] columnFilter,
                                   ConfigurationInputFileEAV eav,
                                   Boolean useMeasureTimestampReliability,
                                   Map<String, ConfigurationValueMapping> valueMappings,
                                   boolean tmdlUseTimestamp,
                                   boolean noSummary) {

        this.charset = charset;
        this.mappingColumns = mappingColumns;
        this.file = file;
        this.key = key;
        this.name = name;
        this.whitelist = whitelist;
        this.blacklist = blacklist;
        this.hasEncounter = hasEncounter;
        this.start = start;
        this.end = end;
        this.dateFormat = dateFormat;
        this.concept = concept;
        this.path = path;
        this.startDay = startDay;
        this.startMonth = startMonth;
        this.startYear = startYear;
        this.endDay = endDay;
        this.endMonth = endMonth;
        this.endYear = endYear;
        this.isFindVisitViaTimestamp = isFindVisitViaTimestamp;
        this.isFindVisitViaTimestampOneToMany = isFindVisitViaTimestampOneToMany;
        this.columnFilter = columnFilter;
        this.eav = eav;
        this.numberConverter = numberConverter;
        this.useMeasureTimestampReliability = useMeasureTimestampReliability;
        this.valueMappings = Collections.unmodifiableMap(valueMappings);
        this.noSummary = noSummary;
        this.tmdlUseTimestamp = tmdlUseTimestamp;
    }

    /**
     * Creates a new instance
     * 
     * @param file
     * @param key
     * @param name
     * @param nonDuplicates
     * @param mappingColumns
     * @param whitelist
     * @param blacklist
     * @param hasEncounter
     * @param start
     * @param end
     * @param dateFormat
     * @param concept
     * @param localeString
     * @param decimalFormatString
     * @param path
     * @param startDay
     * @param startMonth
     * @param startYear
     * @param endDay
     * @param endMonth
     * @param endYear
     * @param isFindVisitViaTimestamp
     * @param isFindVisitViaTimestampOneToMany
     * @param columnFilter
     * @param eav
     * @param useMeasureTimestampReliability
     * @param valueMappings
     * @param useTimestamp
     * @param noSummary
     */
    protected ConfigurationInputFile(Set<File> file,
                                     ConfigurationKey key,
                                     String name,
                                     Map<String, String> mappingColumns,
                                     Set<String> whitelist,
                                     Set<String> blacklist,
                                     boolean hasEncounter,
                                     String start,
                                     String end,
                                     String dateFormat,
                                     String concept,
                                     String localeString,
                                     String decimalFormatString,
                                     String path,
                                     String startDay,
                                     String startMonth,
                                     String startYear,
                                     String endDay,
                                     String endMonth,
                                     String endYear,
                                     boolean isFindVisitViaTimestamp,
                                     boolean isFindVisitViaTimestampOneToMany,
                                     Charset charset,
                                     ConfigurationInputFileColumnFilter[] columnFilter,
                                     ConfigurationInputFileEAV eav,
                                     Boolean useMeasureTimestampReliability,
                                     Map<String, ConfigurationValueMapping> valueMappings,
                                     boolean tmdlUseTimestamp,
                                     boolean noSummary) {

        this.charset = charset;
        this.mappingColumns = mappingColumns;
        this.file = file;
        this.key = key == null ? null : key.map(mappingColumns);
        this.name = name;
        this.whitelist = map(whitelist, mappingColumns);
        this.blacklist = map(blacklist, mappingColumns);
        this.hasEncounter = hasEncounter;
        this.start = map(start, mappingColumns);
        this.end = map(end, mappingColumns);
        this.dateFormat = dateFormat;
        this.concept = map(concept, mappingColumns);
        this.path = path;
        this.startDay = map(startDay, mappingColumns);
        this.startMonth = map(startMonth, mappingColumns);
        this.startYear = map(startYear, mappingColumns);
        this.endDay = map(endDay, mappingColumns);
        this.endMonth = map(endMonth, mappingColumns);
        this.endYear = map(endYear, mappingColumns);
        this.isFindVisitViaTimestamp = isFindVisitViaTimestamp;
        this.columnFilter = map(columnFilter, mappingColumns);
        this.eav = eav == null ? null : eav.map(mappingColumns);
        this.isFindVisitViaTimestampOneToMany = isFindVisitViaTimestampOneToMany;
        this.valueMappings = Collections.unmodifiableMap(valueMappings);
        this.noSummary = noSummary;

        if (localeString != null && decimalFormatString != null) {
            this.numberConverter = new ConfigurationInputFileNumberFormat(decimalFormatString,
                                                                          localeString);
        } else if (decimalFormatString != null) {
            this.numberConverter = new ConfigurationInputFileNumberFormat(decimalFormatString);
        } else {
            this.numberConverter = new ConfigurationInputFileNumberFormat();
        }

        this.useMeasureTimestampReliability = useMeasureTimestampReliability;
        this.tmdlUseTimestamp = tmdlUseTimestamp;
    }

    /**
     * Gets a clone of the configuration input file object.
     * 
     * @param inputConfig
     * @param name
     * @param combinations
     * @return
     */
    @SuppressWarnings("unchecked")
    public ConfigurationInputFile createEAVEntity(ConfigurationInputFileEAV inputConfig,
                                                  String name,
                                                  Set<List<String>> combinations) {

        // Name
        String concept = inputConfig.getValue();

        // Configure filters
        int index = 0;
        ConfigurationInputFileColumnFilter[] filters = new ConfigurationInputFileColumnFilter[combinations.size()];
        for (List<String> combination : combinations) {
            filters[index++] = new ConfigurationInputFileColumnFilter(inputConfig.getEntity(),
                                                                      combination);
        }

        // Create config
        ConfigurationInputFileEAV eavConfig = new ConfigurationInputFileEAV(inputConfig.getEntity(),
                                                                            inputConfig.getValue(),
                                                                            inputConfig.getAttribute(),
                                                                            inputConfig.isExplicit());

        // Removes all attributes from the attributelist list.
        // Needed for generically created entities so that the concept/modifier
        // of their template entity
        // won't be used as a modifier.
        // TODO: This probably doesnt work for EAV combined with blacklist
        for (String attribute : eavConfig.getEntity()) {
            whitelist.remove(attribute);
            blacklist.remove(attribute);
        }

        // Return
        return new ConfigurationInputFile(file,
                                          key.clone(),
                                          name,
                                          (Map<String, String>) ((HashMap<String, String>) mappingColumns).clone(),
                                          whitelist,
                                          blacklist,
                                          hasEncounter,
                                          start,
                                          end,
                                          dateFormat,
                                          concept,
                                          numberConverter,
                                          path,
                                          startDay,
                                          startMonth,
                                          startYear,
                                          endDay,
                                          endMonth,
                                          endYear,
                                          isFindVisitViaTimestamp,
                                          isFindVisitViaTimestampOneToMany,
                                          charset,
                                          filters,
                                          eavConfig,
                                          useMeasureTimestampReliability,
                                          valueMappings,
                                          tmdlUseTimestamp,
                                          noSummary);
    }

    /**
     * Returns the blacklist
     * 
     * @return
     */
    public Set<String> getBlacklist() {
        return blacklist;
    }

    /**
     * Returns the charset, if any
     * 
     * @return
     */
    public Charset getCharset() {
        return charset;
    }

    /**
     * Get the column filter
     * 
     * @return a column filter object or null
     */
    public ConfigurationInputFileColumnFilter[] getColumnFilter() {
        return columnFilter;
    }

    /**
     * Returns the associated EAV configuration
     * 
     * @return
     */
    public ConfigurationInputFileEAV getEAV() {
        return eav;
    }

    /**
     * Gets the name of the entity which was used to configure the generic
     * entity, i.e. it returns the name of the entity which uses the EAV
     * property.
     * 
     * @return - a string
     */
    public String getEAVEntityName() {
        return eav.getAttribute();
    }

    /**
     * Returns the file names
     * 
     * @return
     */
    public String getFileNames() {
        String fileNames = "";
        for (File f : file) {
            fileNames += f.getName() + " ";
        }
        return fileNames;
    }

    /**
     * Returns the key
     * 
     * @return
     */
    public ConfigurationKey getKey() {
        return key;
    }

    /**
     * Returns the mapping TODO: Rename to getMapping() when done
     * 
     * @return
     */
    public Map<String, String> getMapping() {
        return this.mappingColumns;
    }

    /**
     * Returns the name
     * 
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Get the number converter
     * 
     * @return
     */
    public ConfigurationInputFileNumberFormat getNumberConverter() {
        return numberConverter;
    }

    /**
     * Returns whether the file has a path
     * 
     * @return
     */
    public String getPath() {
        if (path == null) {
            return null;
        } else {
            return path.split("\\+")[0];
        }
    }

    /**
     * Returns the whitelist
     * 
     * @return
     */
    public Set<String> getWhitelist() {
        return whitelist;
    }

    /**
     * Is there a column filter
     * 
     * @return
     */
    public boolean hasColumnFilter() {
        return columnFilter != null;
    }

    /**
     * Returns whether there is a single concept specified for this file or not
     * 
     * @return boolean
     */
    public boolean hasConceptSpecified() {
        return concept != null;
    }

    /**
     * Returns whether there is an encounter specified for this file or not
     * 
     * @return hasEncounter
     */
    public boolean hasEncounter() {
        return hasEncounter;
    }

    /**
     * It's possible that the end date is not written as a date in a single
     * column. It may be splitten in seveal columns: day, month, year
     * 
     * @return
     */
    public boolean hasSeveralColumnsForEndDate() {
        return endDay != null || endMonth != null || endYear != null;
    }

    /**
     * It's possible that the start date is not written as a date in a single
     * column. It may be splitten in seveal columns: day, month, year
     * 
     * @return
     */
    public boolean hasSeveralColumnsForStartDate() {
        return startDay != null || startMonth != null || startYear != null;
    }

    /**
     * Returns true, if the configuration input file is configured eav explicit
     * 
     * @return - a boolean
     */
    public boolean isEAV() {
        return eav != null;
    }

    /**
     * Returns whether to find the visit of an observation via the start and end
     * date
     * 
     * @return a boolean
     */
    public boolean isFindVisitViaTimestamp() {
        return isFindVisitViaTimestamp;
    }

    /**
     * Returns whether to find the visit of an observation via the start and end
     * date and to map data to all visits that match
     * 
     * @return a boolean
     */
    public boolean isFindVisitViaTimestampOneToMany() {
        return isFindVisitViaTimestampOneToMany;
    }

    /**
     * Gets whether to use the measure for timestamp reliability.
     * 
     * @return
     */
    public boolean isMeasureTimestampReliabilityToUse() {
        return useMeasureTimestampReliability;
    }

    /**
     * Gets whether to use timestamps for each numerical concept.
     * 
     * Specific for tMDataLoader.
     * 
     * @return
     */
    public boolean isTMDLUseTimestamp() {
        return tmdlUseTimestamp;
    }

    /**
     * Maps all filters
     * 
     * @param mappingColumns
     * @param columnFilter
     * @return
     */
    private ConfigurationInputFileColumnFilter[] map(ConfigurationInputFileColumnFilter[] filters,
                                                     Map<String, String> mappingColumns) {
        if (filters == null) { return null; }
        ConfigurationInputFileColumnFilter[] result = new ConfigurationInputFileColumnFilter[filters.length];
        for (int i = 0; i < filters.length; i++) {
            result[i] = filters[i].map(mappingColumns);
        }
        return result;
    }

    /**
     * Applies the mapping
     * 
     * @param set
     * @param mappingColumns
     * @return
     */
    private Set<String> map(Set<String> set, Map<String, String> mappingColumns) {
        if (mappingColumns == null) { return set; }
        Set<String> result = new HashSet<>();
        for (String entry : set) {
            result.add(mappingColumns.getOrDefault(entry, entry));
        }
        return result;
    }

    /**
     * Applies the mapping
     * 
     * @param value
     * @param mappingColumns
     * @return
     */
    private String map(String value, Map<String, String> mappingColumns) {
        if (mappingColumns == null) { return value; }
        return mappingColumns.getOrDefault(value, value);
    }
}
