/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.configuration;

/**
 * The class VisitSelector.
 * 
 * Represents the configuration of the visit selector. The visit selector is
 * optional and may be configured in configuration.properties.
 * 
 * @author Claudia Lang
 * @author Fabian Prasser
 */
public class ConfigurationVisitSelector {

    /** Require all visits */
    public static boolean ALL_VISITS_REQUIRED;

    /** Merge visits within a selector */
    public static boolean MERGE;

    /** The offset of month to select after the first visit. */
    private final int     startMonth;

    /** The deviation of the date of the visit from the start date. */
    private final int     endMonth;

    /**
     * Optional. To rename the visit. The default name of a visit is Visit-<N> N
     * is the number of appearance. N has no chronical function.
     */
    private final String  name;

    /**
     * Creates the visit selector
     * 
     * @param startMonth
     * @param endMonth
     * @param name
     */
    public ConfigurationVisitSelector(int startMonth, int endMonth, String name) {
        this.startMonth = startMonth;
        this.endMonth = endMonth;
        this.name = name;
    }

    /**
     * Returns the month
     * 
     * @return
     */
    public int getEndMonth() {
        return endMonth;
    }

    /**
     * Returns the name of the visit
     * 
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the month
     * 
     * @return
     */
    public int getStartMonth() {
        return startMonth;
    }
}
