/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * The class EAV.
 * 
 * Representation of the propery for generic entities.
 * 
 * @author Claudia Lang
 *
 */
public class ConfigurationInputFileEAV {

    /**
     * The name of the attribute to use for a new entity. For each value of this
     * attribute one new entity will be created generically.
     */
    private final List<String> entity;

    /**
     * A name of the attribute which will be the concept in the generic entity.
     */
    private final String       value;

    /**
     * The name of the entity (File.Name), which was configured to get generic
     * entities.
     */
    private final String       attribute;

    /** Is this an explicit entity */
    private final boolean      explicit;

    /**
     * Creates a new instance
     * 
     * @param entity
     *            - the name of the attribute to use to create a new entity
     *            generically.
     * @param value
     *            - the name of an attribute to use as a concept of the new
     *            entity.
     */
    public ConfigurationInputFileEAV(List<String> entity,
                                     String value,
                                     String attribute,
                                     boolean explicit) {
        this.entity = entity;
        this.value = value;
        this.attribute = attribute;
        this.explicit = explicit;
    }

    /**
     * Gets a clone of the eav object
     */
    @Override
    public ConfigurationInputFileEAV clone() {
        return new ConfigurationInputFileEAV(entity, value, attribute, explicit);
    }

    /**
     * Returns the attribute
     * 
     * @return
     */
    public String getAttribute() {
        return this.attribute;
    }

    /**
     * Returns the entity
     * 
     * @return
     */
    public List<String> getEntity() {
        return this.entity;
    }

    /**
     * Returns the value
     * 
     * @return
     */
    public String getValue() {
        return this.value;
    }

    /**
     * Explicit
     * 
     * @return
     */
    public boolean isExplicit() {
        return this.explicit;
    }

    /**
     * Applies the mapping
     * 
     * @param mappingColumns
     * @return
     */
    public ConfigurationInputFileEAV map(Map<String, String> mappingColumns) {
        if (mappingColumns == null) { return this; }
        List<String> entity = new ArrayList<>();
        for (String entry : this.entity) {
            entity.add(mappingColumns.getOrDefault(entry, entry));
        }
        return new ConfigurationInputFileEAV(entity, value, attribute, explicit);
    }
}
