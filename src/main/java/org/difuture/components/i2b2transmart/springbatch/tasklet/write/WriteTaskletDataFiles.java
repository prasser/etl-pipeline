/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.springbatch.tasklet.write;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.difuture.components.i2b2transmart.configuration.ConfigurationInputFile;
import org.difuture.components.i2b2transmart.model.Data;
import org.difuture.components.i2b2transmart.model.Model;
import org.difuture.components.i2b2transmart.model.Subject;
import org.difuture.components.i2b2transmart.model.Visit;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

/**
 * Writes data files. Used in a step to write the data files (params files,
 * properties file, Visit file, Subject file, Attributes files).
 * 
 * @author Claudia Lang
 * 
 */
public abstract class WriteTaskletDataFiles implements Tasklet, InitializingBean {

    /** Folder in which to write the files. */
    protected File folder;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(folder, "folder must not be null!");
    }

    @Override
    public RepeatStatus execute(StepContribution contribution,
                                ChunkContext chunkContext) throws Exception {
        write();
        return RepeatStatus.FINISHED;
    }

    /**
     * Sets the folder.
     *
     * <BR>
     * TODO why do we need this method? Why not set this.folder from within the
     * constructor?
     * 
     * @param folder
     *            the new folder
     */
    public void setFolder(File folder) {
        this.folder = folder;
    }

    /**
     * Returns all observation names (concept followed by modifiers)
     * 
     * @param config
     * @param concept
     * @return
     */
    protected List<String> getAttributeNames(ConfigurationInputFile config, String concept) {

        // Build list of attribute names to consider
        List<String> attributeNames = new ArrayList<String>();
        attributeNames.add(concept);
        for (String modifier : Model.get().getEntity(config).getModifiers()) {
            if (isDataAvailableForAttribute(config, modifier)) {
                attributeNames.add(modifier);
            }
        }

        // Done
        return attributeNames;
    }

    /**
     * Returns whether there are values for this attribute.
     * 
     * @param config
     *            a Map containing the Attributes objects
     * @param attribute
     *            the name of the attribute
     *
     * @return HashSet
     */
    protected boolean isDataAvailableForAttribute(ConfigurationInputFile config, String attribute) {
        for (Subject subject : Model.get().getSubjects()) {
            for (Visit visit : subject.getVisits()) {

                // Get
                Set<Data> data = visit.getData(config);

                // Skip
                if (data == null) {
                    continue;
                }

                // Check
                for (Data dataPoint : data) {
                    String value = dataPoint.getValues().get(attribute);
                    if (!isEmpty(value)) { return true; }
                }
            }
        }
        return false;
    }

    /**
     * Returns whether the value is empty
     * 
     * @param value
     * @return
     */
    protected boolean isEmpty(String value) {
        return value == null || value.equals("");
    }

    /**
     * Writes the staging files
     * 
     * @throws IOException
     */
    protected abstract void write() throws IOException;
}
