/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.springbatch.tasklet.write;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.difuture.components.i2b2transmart.configuration.ConfigurationInputFile;
import org.difuture.components.i2b2transmart.configuration.ConfigurationSubject;
import org.difuture.components.i2b2transmart.model.AttributeAge;
import org.difuture.components.i2b2transmart.model.Model;
import org.difuture.components.i2b2transmart.model.OntologyNodeConcept;
import org.difuture.components.i2b2transmart.model.Subject;
import org.difuture.components.i2b2transmart.model.Timestamp;
import org.difuture.components.i2b2transmart.model.Visit;
import org.difuture.components.util.FileUtil;

/**
 * Writes the mapping file
 * 
 * @author Claudia Lang
 * 
 */
public class WriteTaskletMappingFileI2b2 extends WriteTaskletMappingFile {

    /**
     * Gets all rows to write in the mapping file for each concept
     * 
     * @param concept
     * @param indexMappingFileMod
     * @param config
     * @return a list with string arrays
     */
    private List<String[]> getConceptNodesInMappingFile(String concept,
                                                        Integer indexMappingFileMod,
                                                        ConfigurationInputFile config) {

        List<String[]> result = new ArrayList<>();
        String placeholder = "";
        String fileName = FileUtil.escapeFileName(config, concept);
        Map<String, OntologyNodeConcept> conceptMap = Model.get()
                                                           .getOntology()
                                                           .getOntologyNodesForOutputColumns(config);
        List<String> outputColumnNames = Model.get()
                                              .getEntity(config)
                                              .getOutputColumnNames()
                                              .get(concept);
        OntologyNodeConcept conceptNode = Model.get()
                                               .getOntology()
                                               .getOntologiesForConcepts(config)
                                               .get(concept);

        // Get the column information for the identifying data (subject, visit,
        // start, end)
        result.addAll(getEntriesInMappingFileIdentifiers(fileName));

        // TODO: This happens, if there are no data available for this concept.
        if (conceptNode == null) { return result; }

        Map<String, String[]> mappingDataCon = new HashMap<>();
        Map<String, String[]> mappingDataMod = new HashMap<>();

        for (int i = 0; i < outputColumnNames.size(); i++) {
            String outputColumnName = outputColumnNames.get(i);

            // It is possible, that the concept name and the modifier name
            // start with the same name, so we need to test the type, too
            OntologyNodeConcept node = conceptMap.get(outputColumnName);

            // If there is no node, there is no data
            if (node == null) {
                continue;
            }

            String nodeDataType = node.isNumeric() ? "number" : "text";
            boolean nodeIsConcept = node.getType(node).equals("concept") ? true : false;
            if (nodeIsConcept) {
                mappingDataCon.put(outputColumnName,
                                   getRow(fileName,
                                          indexMappingFileMod + i,
                                          false,
                                          "CON:" + conceptMap.get(outputColumnName).getCode(),
                                          nodeDataType,
                                          placeholder));
            } else {
                mappingDataMod.put(outputColumnName,
                                   getRow(fileName,
                                          indexMappingFileMod + i,
                                          false,
                                          "MOD:" + conceptMap.get(outputColumnName).getCode(),
                                          nodeDataType,
                                          placeholder));
            }
        }

        if (Model.get().isI2b2() && config.isMeasureTimestampReliabilityToUse()) {
            int startIndex = outputColumnNames.size();
            for (int i = startIndex; i < startIndex + Timestamp.Reliability.values().length; i++) {
                String outputColumnName = Timestamp.Reliability.values()[i - startIndex].toString();
                mappingDataMod.put(outputColumnName,
                                   getRow(fileName,
                                          indexMappingFileMod + i,
                                          false,
                                          "MOD:" + conceptMap.get(outputColumnName).getCode(),
                                          "text",
                                          placeholder));
            }
        }

        // Get the column information for the concepts and modifiers
        for (Entry<String, String[]> conEntry : mappingDataCon.entrySet()) {
            result.add(conEntry.getValue());
            for (Entry<String, String[]> modEntry : mappingDataMod.entrySet()) {
                result.add(modEntry.getValue());
            }
        }

        return result;
    }

    /**
     * Gets all rows to write in the mapping file for the observation and their
     * concepts and modidfiers
     * 
     * @return a list with string arrays
     */
    private List<String[]> getEntriesForConceptNodes() {
        List<String[]> result = new ArrayList<>();
        // Pos 0: subjectId
        // Pos 1: visitId
        // Pos 2: start
        // Pos 3: end
        // >= Pos 4: value (an offset is added to get the correct column number)
        // Note: The indexes for the columns start in component transmart batch
        // at 1 instead of 0
        Integer indexMappingFileMod = 5;

        // For each input file and concept
        for (ConfigurationInputFile config : Model.get().getConfig().getFiles()) {
            for (String concept : Model.get().getEntity(config).getConcepts()) {
                result.addAll(getConceptNodesInMappingFile(concept, indexMappingFileMod, config));
            }
        }

        return result;
    }

    /**
     * Gets all rows to write in the mapping file for the identifiers
     * 
     * @param fileName
     *            - the name of the file in which to find the entries - This is
     *            not the file name of the mapping file
     * @return a list with string arrays
     */
    private List<String[]> getEntriesInMappingFileIdentifiers(String fileName) {
        List<String[]> result = new ArrayList<>();
        String placeholder = "";

        result.add(getRow(fileName, 1, true, "PAT:EID", placeholder, placeholder));
        result.add(getRow(fileName, 2, false, "VIS:EID", placeholder, placeholder));
        result.add(getRow(fileName, 3, true, "START_DATE", placeholder, placeholder));
        result.add(getRow(fileName, 4, false, "END_DATE", placeholder, placeholder));

        return result;
    }

    /**
     * Gets all rows to write in the mapping file for the subjects
     * 
     * @return a list of string arrays
     */
    private List<String[]> getEntriesInMappingFileSubjects() {
        List<String[]> result = new ArrayList<>();
        String placeholder = "";
        String fileName = "___subjects___.csv";

        // Prepare subjects
        ConfigurationSubject config = Model.get().getConfig().getSubjectConfiguration();
        int column = 1;

        // Write subject id (mandatory)
        result.add(getRow(fileName, column++, true, "PAT:EID", placeholder, placeholder));

        // Write optional attributes
        if (config.sex != null) {
            result.add(getRow(fileName, column++, false, "PAT:SEX", placeholder, placeholder));
        }
        if (config.zip != null) {
            result.add(getRow(fileName, column++, false, "PAT:ZIP", placeholder, placeholder));
        }
        if (config.age != null) {
            result.add(getRow(fileName,
                              column++,
                              false,
                              "PAT:AGE_IN_YEARS_NUM",
                              placeholder,
                              placeholder));
        }
        if (config.birthDate != null) {
            result.add(getRow(fileName,
                              column++,
                              false,
                              "PAT:BIRTH_DATE",
                              placeholder,
                              placeholder));
        }
        if (config.vitalStatus != null) {
            result.add(getRow(fileName,
                              column++,
                              false,
                              "PAT:VITAL_STATUS",
                              placeholder,
                              placeholder));
        }
        if (config.language != null) {
            result.add(getRow(fileName, column++, false, "PAT:LANGUAGE", placeholder, placeholder));
        }
        if (config.race != null) {
            result.add(getRow(fileName, column++, false, "PAT:RACE", placeholder, placeholder));
        }
        if (config.maritalStatus != null) {
            result.add(getRow(fileName,
                              column++,
                              false,
                              "PAT:MARITAL_STATUS",
                              placeholder,
                              placeholder));
        }
        if (config.religion != null) {
            result.add(getRow(fileName, column++, false, "PAT:RELIGION", placeholder, placeholder));
        }
        if (config.stateCityZipPath != null) {
            result.add(getRow(fileName,
                              column++,
                              false,
                              "PAT:STATECITYZIP_PATH",
                              placeholder,
                              placeholder));
        }
        if (config.income != null) {
            result.add(getRow(fileName, column++, false, "PAT:INCOME", placeholder, placeholder));
        }
        if (config.blob != null) {
            result.add(getRow(fileName, column++, false, "PAT:BLOB", placeholder, placeholder));
        }

        return result;
    }

    /**
     * Gets all rows to write in the mapping file for the visits (encounters)
     * 
     * @return a list with string arrays
     */
    private List<String[]> getEntriesInMappingFileVisits() {
        List<String[]> result = new ArrayList<>();
        String placeholder = "";
        String fileName = "___visits___.csv";

        // Prepare visits
        Visit first = Model.get().getVisits().iterator().next();
        int column = 1;

        // Mandatory entries
        result.add(getRow(fileName, column++, true, "PAT:EID", placeholder, placeholder));
        result.add(getRow(fileName, column++, true, "VIS:EID", placeholder, placeholder));
        result.add(getRow(fileName, column++, false, "VIS:START_DATE", placeholder, placeholder));

        // Optional entries
        if (first.getEnd() != null) {
            result.add(getRow(fileName, column++, false, "VIS:END_DATE", placeholder, placeholder));
        }
        if (first.getActiveStatus() != null) {
            result.add(getRow(fileName,
                              column++,
                              false,
                              "VIS:ACTIVE_STATUS",
                              placeholder,
                              placeholder));
        }
        if (first.getInout() != null) {
            result.add(getRow(fileName, column++, false, "VIS:INOUT", placeholder, placeholder));
        }
        if (first.getLocation() != null) {
            result.add(getRow(fileName, column++, false, "VIS:LOCATION", placeholder, placeholder));
        }
        if (first.getLocationPath() != null) {
            result.add(getRow(fileName,
                              column++,
                              false,
                              "VIS:LOCATION_PATH",
                              placeholder,
                              placeholder));
        }
        if (first.getLengthOfStay() != null) {
            result.add(getRow(fileName,
                              column++,
                              false,
                              "VIS:LENGTH_OF_STAY",
                              placeholder,
                              placeholder));
        }
        if (first.getBlob() != null) {
            result.add(getRow(fileName, column++, false, "VIS:BLOB", placeholder, placeholder));
        }

        return result;
    }

    /**
     * Gets a single row to write in the mapping file
     * 
     * @param filename
     *            - the name of the file in which to find the entry (e.g.
     *            subject, visit, observation) - This is not the file name of
     *            the mapping file
     * @param offset
     *            - the number of the column (starting at 1) in which to find
     *            the value for this field
     * @param mandatory
     *            - whether this field is optional or not
     * @param concept
     *            - the name of the concept (observation)
     * @param type
     *            - the type of the concept (number or text)
     * @return a string array
     */
    private String[] getRow(String filename,
                            int offset,
                            boolean mandatory,
                            String concept,
                            String type,
                            String unit) {
        return new String[] { filename,
                              String.valueOf(offset),
                              String.valueOf(mandatory),
                              concept,
                              type,
                              unit };
    }

    /**
     * Gets all rows to write in the mapping file for the age of the subjects
     * 
     * @return a list with string arrays
     */
    private List<String[]> getSubjectsMappingAge() {
        List<String[]> result = new ArrayList<>();
        String placeholder = "";
        String fileName = "___subjects_age___.csv";
        int column = 1;

        // Write concept age (mandatory attributes)
        result.add(getRow(fileName, column++, true, "PAT:EID", placeholder, placeholder));
        result.add(getRow(fileName, column++, true, "START_DATE", placeholder, placeholder));

        // Concepts for valid values
        for (int i = 0; i <= AttributeAge.MAX_VALUE; i++) {
            result.add(getRow(fileName, column++, false, "CON:DEM|AGE:" + i, "text", placeholder));
        }

        // Concepts for invalid values
        result.add(getRow(fileName, column++, false, "CON:DEM|AGE:u", "text", placeholder));
        result.add(getRow(fileName, column++, false, "CON:DEM|AGE:@", "text", placeholder));

        return result;
    }

    /**
     * Gets all rows to write in the mapping file for the gender of the subjects
     * 
     * @return a list with string arrays
     */
    private List<String[]> getSubjectsMappingGender() {
        ConfigurationSubject config = Model.get().getConfig().getSubjectConfiguration();
        List<String[]> result = new ArrayList<>();
        String placeholder = "";
        String fileName = "___subjects_gender___.csv";

        // Write concept gender
        result.add(getRow(fileName, 1, true, "PAT:EID", placeholder, placeholder));
        result.add(getRow(fileName, 2, true, "START_DATE", placeholder, placeholder));
        if (config.sex != null) {
            result.add(getRow(fileName, 3, false, "CON:DEM|SEX:f", "text", placeholder));
            result.add(getRow(fileName, 4, false, "CON:DEM|SEX:m", "text", placeholder));
            result.add(getRow(fileName, 5, false, "CON:DEM|SEX:u", "text", placeholder));
        } else {
            result.add(getRow(fileName, 6, false, "CON:DEM|SEX:@", "text", placeholder));
        }
        return result;
    }

    /**
     * Gets all rows to write in the mapping file for the age of the subjects
     * 
     * @return a list with string arrays
     */
    private List<String[]> getSubjectsMappingID() {
        List<String[]> result = new ArrayList<>();
        String placeholder = "";
        String fileName = "___subjects_id___.csv";
        int column = 1;

        // Write concept (mandatory attributes)
        result.add(getRow(fileName, column++, true, "PAT:EID", placeholder, placeholder));
        result.add(getRow(fileName, column++, true, "START_DATE", placeholder, placeholder));

        // Concepts for valid values
        for (Subject subject : Model.get().getSubjects()) {
            String id = subject.getId().toString();
            result.add(getRow(fileName, column++, false, "CON:SYS|ID:" + id, "text", placeholder));
        }

        // Done
        return result;
    }

    /**
     * Gets all rows to write in the mapping file for the race of the subjects.
     * So far only 'Not recorded' is supported.
     * 
     * @return a list with string arrays
     */
    private List<String[]> getSubjectsMappingRace() {
        List<String[]> result = new ArrayList<>();
        String placeholder = "";
        String fileName = "___subjects_race___.csv";

        // Write concept race
        // So far only "unknown" is supported.
        // It is necessary for the breakdown-analyses in i2b2
        result.add(getRow(fileName, 1, true, "PAT:EID", placeholder, placeholder));
        result.add(getRow(fileName, 2, true, "START_DATE", placeholder, placeholder));
        result.add(getRow(fileName, 3, false, "CON:DEM|RACE:@", "text", placeholder));
        /*
         * int index = 3; for (String concept :
         * I2b2TableContents.CODE_LOOKUP_PAT_RACE.keySet()) {
         * result.add(getRow(fileName, index++, false, "CON:"+concept, "text",
         * placeholder)); }
         */

        return result;
    }

    /**
     * Gets all rows to write in the mapping file for the vital status of the
     * subjects. So far only 'Not recorded' is supported.
     * 
     * @return a list with string arrays
     */
    private List<String[]> getSubjectsMappingVitalStatus() {
        List<String[]> result = new ArrayList<>();
        String placeholder = "";
        String fileName = "___subjects_vital_status___.csv";

        // Write concept vital status
        // It is necessary for the breakdown-analyses in i2b2
        result.add(getRow(fileName, 1, true, "PAT:EID", placeholder, placeholder));
        result.add(getRow(fileName, 2, true, "START_DATE", placeholder, placeholder));
        result.add(getRow(fileName, 3, false, "CON:DEM|VITAL:@", "text", placeholder));
        /*
         * int index = 3; for (String concept :
         * I2b2TableContents.CODE_LOOKUP_PAT_VIT.keySet()) {
         * result.add(getRow(fileName, index++, false, "CON:" + concept, "text",
         * placeholder)); }
         */

        return result;
    }

    /**
     * Gets all rows to write in the mapping file
     * 
     * @return a list of string arrays
     */
    @Override
    protected List<String[]> getEntriesToMappingFile() {

        // Prepare
        List<String[]> rows = new ArrayList<>();

        // Add
        rows.addAll(getEntriesInMappingFileSubjects());
        rows.addAll(getSubjectsMappingAge());
        rows.addAll(getSubjectsMappingGender());
        rows.addAll(getSubjectsMappingRace());
        rows.addAll(getSubjectsMappingVitalStatus());
        rows.addAll(getEntriesInMappingFileVisits());
        rows.addAll(getEntriesForConceptNodes());
        if (Model.get().getConfig().getSubjectConfiguration().loadIDs) {
            rows.addAll(getSubjectsMappingID());
        }

        // Done
        return rows;
    }

    /**
     * Returns the header for the mapping file
     * 
     * @return a string array
     */
    @Override
    protected String[] getHeader() {
        return new String[] { "FILENAME",
                              "COLUMN_NUMBER",
                              "MANDATORY",
                              "VARIABLE",
                              "TYPE",
                              "UNIT" };
    }

}
