/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.springbatch.tasklet;

import java.time.Period;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.difuture.components.i2b2transmart.configuration.ConfigurationSubject;
import org.difuture.components.i2b2transmart.model.Data;
import org.difuture.components.i2b2transmart.model.Model;
import org.difuture.components.i2b2transmart.model.Subject;
import org.difuture.components.i2b2transmart.model.Timestamp;
import org.difuture.components.i2b2transmart.model.Visit;
import org.difuture.components.util.LoggerUtil;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

/**
 * Assigns to each visit a start date and an end date, in the corresponding
 * order.
 * 
 * 1.) Preferable, the start and end dates of the attributes assigned to the
 * visit will be used. 2.) If the attributes don't have any start and/or end
 * date, the start date of the visit (found in the visit file) will be used. 3.)
 * If neither the attributes nor the visit has got a start date, the dummy date
 * Timestamp.now() will be used.
 * 
 * The start and end dates of the visits will then be use to get the date of the
 * first visit for each subject. If configured, calculates the ages of the
 * subjects, basing on the date of the first visit.
 * 
 * @author Claudia Lang
 */
public class TaskletCalculateTimestamps implements Tasklet {

    /** Artificial date used if no other date is available */
    public static final Timestamp TIME_NOW = Timestamp.now();

    @Override
    public RepeatStatus execute(StepContribution stepContribution,
                                ChunkContext chunkContext) throws Exception {

        // Assign start and end dates to visit
        assignDatesToVisits();

        // Assign start and end dates to data
        assignDatesToData();

        // Assign to subjects the start dates of their first visits
        assignFirstDateToSubjects();

        // Assign calculated the ages of the subjects
        assignAgeToSubjects();

        // Done
        return RepeatStatus.FINISHED;
    }

    /**
     * Calculates the age of a subject, basing on the first visit.
     */
    private void assignAgeToSubjects() {

        // Subject config
        ConfigurationSubject config = Model.get().getConfig().getSubjectConfiguration();

        // No field for age exists and its not required to calculate the age
        if (config.age == null) { return; }

        // The field for the age exists and so it's not necessary to calculate
        // the age
        if (!config.age.equals("CALCULATE")) { return; }

        // It is required to calculate the age but no field for the date of
        // birth has been defined
        if (config.birthDate == null) {
            LoggerUtil.warn("Age of patients cannot be calculated because not field for the birth date is configured.",
                            this.getClass());
            return;
        }

        // Calculate age for all subjects
        for (Subject subject : Model.get().getSubjects()) {

            // Skip
            if (subject.getDateFirstVisit() == null || !subject.getAge().isMissing() ||
                subject.getBirthDate() == null) {
                continue;
            }

            // Calculate age
            int age = Period.between(subject.getBirthDate(),
                                     subject.getDateFirstVisit().toLocalDate())
                            .getYears();

            // NOTE: The age is used for the concepts. In i2b2 these are
            // categorical, so they can be used for the break down analyses.
            subject.getAge().setValue(age);
        }
    }

    /**
     * Assigns start and end dates to data, basing on start and end dates of the
     * visits.
     */
    private void assignDatesToData() {

        // Prepare
        int count = 0;
        int countDummy = 0;

        // For each subject
        for (Subject subject : Model.get().getSubjects()) {

            // For each (artificial) visit
            for (Visit visit : subject.getVisits()) {

                // For each set of data points per entity
                for (Set<Data> set : visit.getData().values()) {

                    // Prepare modification of data objects
                    List<Data> updated = new ArrayList<>();
                    Iterator<Data> iterator = set.iterator();

                    // For each data object
                    while (iterator.hasNext()) {

                        // Prepare
                        Data data = iterator.next();

                        // Assign start
                        if (visit.getStart() != null && visit.getStart().isValid()) {
                            if (data != null &&
                                (data.getStart() == null || !data.getStart().isValid())) {
                                handleElementChange(set, iterator, updated, data); // Needs
                                                                                   // to
                                                                                   // be
                                                                                   // done
                                                                                   // because
                                                                                   // modified
                                                                                   // dates
                                                                                   // changes
                                                                                   // the
                                                                                   // hash
                                                                                   // code
                                data.setStart(visit.getStart());
                                if (data.getStart().equals(TIME_NOW)) {
                                    countDummy++;
                                } else {
                                    count++;
                                }
                            }
                        }

                        // Assign end
                        if (visit.getEnd() != null && visit.getEnd().isValid()) {
                            if (data != null &&
                                (data.getEnd() == null || !data.getEnd().isValid())) {
                                handleElementChange(set, iterator, updated, data); // Needs
                                                                                   // to
                                                                                   // be
                                                                                   // done
                                                                                   // because
                                                                                   // modified
                                                                                   // dates
                                                                                   // changes
                                                                                   // the
                                                                                   // hash
                                                                                   // code
                                data.setEnd(visit.getEnd());
                                if (data.getEnd().equals(TIME_NOW)) {
                                    countDummy++;
                                } else {
                                    count++;
                                }
                            }
                        }
                    }

                    // Add updated back again
                    set.addAll(updated);
                }
            }
        }

        LoggerUtil.info("Found via visits " + count +
                        " start and end dates for entities without start/end date. " + countDummy +
                        " dummy dates were assigned due to missing dates in the visits.",
                        this.getClass());
    }

    /**
     * Assigns start and end dates from data points to visits, if visits are
     * missing timestamps. If they are not available, too, a dummy date will be
     * used.
     */
    private void assignDatesToVisits() {

        // Prepare
        int count = 0;
        int countDummy = 0;

        // For each subject
        for (Subject subject : Model.get().getSubjects()) {

            // Prepare
            Set<Visit> set = subject.getVisits();
            List<Visit> updated = new ArrayList<>();
            Iterator<Visit> iterator = set.iterator();

            // For each (artificial) visit
            // Note: Each subject has either no artificial visit or exactly one.
            while (iterator.hasNext()) {

                // Prepare
                Visit visit = iterator.next();

                // Assign start date
                if (visit.getStart() == null || !visit.getStart().isValid()) {

                    // For each data point
                    for (Data data : visit.getAllData()) {

                        // Check
                        if (data != null && data.getStart() != null && data.getStart().isValid()) {

                            // Initialize start
                            if (visit.getStart() == null || !visit.getStart().isValid()) {
                                handleElementChange(set, iterator, updated, visit); // Needs
                                                                                    // to
                                                                                    // be
                                                                                    // done
                                                                                    // because
                                                                                    // modified
                                                                                    // dates
                                                                                    // changes
                                                                                    // the
                                                                                    // hash
                                                                                    // code
                                visit.setStart(data.getStart());
                                count++;

                                // Update start
                            } else {
                                handleElementChange(set, iterator, updated, visit); // Needs
                                                                                    // to
                                                                                    // be
                                                                                    // done
                                                                                    // because
                                                                                    // modified
                                                                                    // dates
                                                                                    // changes
                                                                                    // the
                                                                                    // hash
                                                                                    // code
                                visit.setStart(data.getStart()
                                                   .isBeforeOrMoreReliable(visit.getStart())
                                                           ? data.getStart()
                                                           : visit.getStart());
                            }
                        }
                    }
                }

                // Assign end date
                if (visit.getEnd() == null || !visit.getEnd().isValid()) {

                    // For each data point
                    for (Data data : visit.getAllData()) {

                        // Check
                        if (data != null && data.getEnd() != null && data.getEnd().isValid()) {

                            // Initialize end
                            if (visit.getEnd() == null || !visit.getEnd().isValid()) {
                                handleElementChange(set, iterator, updated, visit); // Needs
                                                                                    // to
                                                                                    // be
                                                                                    // done
                                                                                    // because
                                                                                    // modified
                                                                                    // dates
                                                                                    // changes
                                                                                    // the
                                                                                    // hash
                                                                                    // code
                                visit.setEnd(data.getEnd());
                                count++;

                                // Update end
                            } else {
                                handleElementChange(set, iterator, updated, visit); // Needs
                                                                                    // to
                                                                                    // be
                                                                                    // done
                                                                                    // because
                                                                                    // modified
                                                                                    // dates
                                                                                    // changes
                                                                                    // the
                                                                                    // hash
                                                                                    // code
                                visit.setEnd(data.getEnd().isAfterOrMoreReliable(visit.getEnd())
                                        ? data.getEnd()
                                        : visit.getEnd());
                            }
                        }
                    }
                }

                // Dummy date if null
                if (visit.getStart() == null || !visit.getStart().isValid()) {
                    handleElementChange(set, iterator, updated, visit); // Needs
                                                                        // to be
                                                                        // done
                                                                        // because
                                                                        // modified
                                                                        // dates
                                                                        // changes
                                                                        // the
                                                                        // hash
                                                                        // code
                    visit.setStart(TIME_NOW);
                    countDummy++;
                }

                // Dummy date if null
                if (visit.getEnd() == null || !visit.getEnd().isValid()) {
                    handleElementChange(set, iterator, updated, visit); // Needs
                                                                        // to be
                                                                        // done
                                                                        // because
                                                                        // modified
                                                                        // dates
                                                                        // changes
                                                                        // the
                                                                        // hash
                                                                        // code
                    visit.setEnd(TIME_NOW);
                    countDummy++;
                }
            }

            // Handle modified visits
            set.addAll(updated);
        }

        // Log
        LoggerUtil.info("Found via entities " + count + " start and end dates for visits. " +
                        countDummy +
                        " dummy dates were assigned due to missing dates in the entities.",
                        this.getClass());
    }

    /**
     * Assigns to each subject the start date of its first visit
     */
    private void assignFirstDateToSubjects() {

        // For each subject
        for (Subject subject : Model.get().getSubjects()) {

            // Start date of the subject's first visit
            Timestamp dateFirstVisit = null;

            // For each non-artificial visit
            for (Visit visit : subject.getVisits()) {

                if (visit.getStart() != null && visit.getStart().isValid() &&
                    !visit.isArtificial()) {

                    // Get start date of the first visit
                    if (dateFirstVisit == null) {
                        dateFirstVisit = visit.getStart();
                    }

                    // Update start date of the first visit
                    if (visit.getStart().isBeforeOrMoreReliable(dateFirstVisit)) {
                        dateFirstVisit = visit.getStart();
                    }
                }
            }

            // Set to now if no timestamp available
            dateFirstVisit = dateFirstVisit != null ? dateFirstVisit : TIME_NOW;

            // Set start date of the first visit of the subject
            subject.setDateFirstVisit(dateFirstVisit);
        }
    }

    /**
     * Generic method to handle when the hash code of elements in the set
     * changes. Updated elements are added to a second set that needs to be
     * re-added later on.
     */
    private <T> void handleElementChange(Set<T> currentElements,
                                         Iterator<T> currentIterator,
                                         List<T> updatedElements,
                                         T element) {
        if (currentElements.contains(element)) {
            currentIterator.remove();
        }
        updatedElements.add(element);
    }
}
