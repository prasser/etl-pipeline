/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.springbatch.tasklet.read;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.Character.UnicodeScript;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.difuture.components.i2b2transmart.configuration.ConfigurationFileFormat;
import org.difuture.components.i2b2transmart.configuration.ConfigurationInputFileColumnFilter;
import org.difuture.components.i2b2transmart.configuration.ConfigurationInputFileNumberFormat;
import org.difuture.components.i2b2transmart.model.Model;
import org.difuture.components.i2b2transmart.model.Timestamp;
import org.difuture.components.util.LoggerUtil;
import org.mozilla.universalchardet.UniversalDetector;

import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import com.univocity.parsers.csv.CsvWriter;
import com.univocity.parsers.csv.CsvWriterSettings;

/**
 * The class CSVReader. Utility class for reading the input files.
 * 
 * @author Claudia Lang
 * @author Fabian Prasser
 * @author Tanmaya Mahapatra
 * @author Ingrid Gatz
 */
public class CSVReader {

    /**
     * A canonical form of a string. Can be used to find strings that are very
     * likely equal but differ due to data quality problems.
     * 
     * @author Fabian Prasser
     */
    public static final class CanonicalString {

        /** Placeholder */
        private static final char PLACEHOLDER = '_';

        /** Original string */
        private final String      original;
        /** Canonical form */
        private final char[]      canonical1;
        /** Canonical form */
        private final String      canonical2;
        /** Canonical form */
        private final String      safeString;
        /** Hash code */
        private final int         hashcode;
        /** Quality: the lower the better */
        private final int         badness;

        /**
         * Creates a new canonical string
         * 
         * @param string
         */
        private CanonicalString(String string) {

            // Store
            this.original = trim(string);

            // Prepare
            StringBuilder build1 = new StringBuilder();
            char[] characters = this.original.toLowerCase().toCharArray();
            boolean wasBad = false;
            int badness = 0;
            int hashcode = 23;

            // Process each character
            for (char c : characters) {

                // Ignore all whitespaces
                if (!isWhitespace(c)) {
                    if (isCanonicalCharacter(c)) {

                        // We calculate the hashcode only on the very basic
                        // letters that must be equal
                        if (isLetterOrNumber(c)) {
                            hashcode = (37 * hashcode) + (int) c;
                        }
                        build1.append(c);
                        wasBad = false;

                    } else if (!wasBad) {

                        badness++;
                        build1.append(PLACEHOLDER);
                        wasBad = true;
                    }
                }
            }

            // Build safe string
            StringBuilder builder2 = new StringBuilder();
            for (Character c : this.original.toCharArray()) {
                if (isDisplayableCharacter(c)) {
                    builder2.append(c);
                } else if (isWhitespace(c)) {
                    builder2.append(' ');
                }
            }
            this.safeString = builder2.toString();
            this.canonical2 = safeString.toLowerCase();

            // Done
            this.badness = badness;
            this.canonical1 = build1.toString().toCharArray();
            this.hashcode = hashcode;
        }

        @Override
        public boolean equals(Object obj) {

            CanonicalString other = (CanonicalString) obj;

            // Check hashcode
            if (hashcode != other.hashcode) return false;

            // Check if second canonical string is equal
            if (canonical2.equals(other.canonical2)) { return true; }

            // Check length of first canonical string
            if (canonical1.length != other.canonical1.length) { return false; }
            // Two strings are considered equal, if they only differ in
            // positions where
            // any of both strings have a placeholder.
            for (int i = 0; i < canonical1.length; i++) {
                char c1 = this.canonical1[i];
                char c2 = other.canonical1[i];
                if (c1 != PLACEHOLDER && c2 != PLACEHOLDER && c1 != c2) { return false; }
            }
            return true;
        }

        /**
         * Returns the canonical strings
         * 
         * @return
         */
        public String getCanonical() {
            return "'" + new String(canonical1) + "'/'" + canonical2 + "'";
        }

        /**
         * Returns the character badness of the original string
         * 
         * @return the characterBadness
         */
        public int getCharacterBadness() {
            return badness;
        }

        /**
         * @return the original
         */
        public String getOriginal() {
            return original;
        }

        /**
         * Returns the safe string
         * 
         * @return
         */
        public String getSafeString() {
            return this.safeString;
        }

        @Override
        public int hashCode() {
            return this.hashcode;
        }
    }

    /** The cache */
    private static Map<String, List<String[]>> CACHE      = new HashMap<String, List<String[]>>();

    /** Supported delimiters */
    private static final char[]                DELIMITERS = { ';', ',', '\t' };

    /**
     * Drops the caches
     */
    public static void dropCaches() {
        LoggerUtil.info("Dropping cache", CSVReader.class);
        CACHE.clear();
    }

    /**
     * Returns a canonical form of a string. Can be used to find strings that
     * are very likely equal but differ due to data quality problems.
     * 
     * @param string
     * @return
     */
    public static CanonicalString getCanonicalForm(String string) {
        return new CanonicalString(string);
    }

    /**
     * Gets for each entry in a list with columns a sets with all different
     * values in this column and returns a list with all sets for the columns.
     * 
     * @param mapping
     * @param columns
     * @param file
     * @param charset
     * @return a list with string-sets
     */
    public static Set<List<String>> getDistinctValues(Map<String, String> mapping,
                                                      List<String> columns,
                                                      File file,
                                                      Charset charset) {

        Set<String> setColumns = new HashSet<>();
        setColumns.addAll(columns);
        List<String[]> rows = getRows(mapping, setColumns, file, charset);

        String[] header = rows.get(0);
        List<Integer> indexes = new ArrayList<>();
        for (String column : columns) {
            for (int i = 0; i < header.length; i++) {
                if (header[i].equals(column)) {
                    indexes.add(i);
                }
            }
        }

        // Prepare
        Set<List<String>> result = new HashSet<>();

        // Skip header
        for (int i = 1; i < rows.size(); i++) {
            String[] row = rows.get(i);
            List<String> rowResult = new ArrayList<>();

            // Add in the given order (in columns)
            for (Integer index : indexes) {
                if (row[index] == null || row[index].equals("")) {
                    continue;
                }
                rowResult.add(row[index]);
            }

            // Add the row only, if no empty value is in any column filtered
            if (rowResult.size() == columns.size()) {
                result.add(rowResult);
            }
        }

        return result;
    }

    /**
     * Gets for each entry in a list with columns a sets with all different
     * values in this column and returns a list with all sets for the columns.
     * 
     * @param mapping
     * @param columns
     * @param files
     * @param charset
     * @return a list with string-sets
     */
    public static Set<List<String>> getDistinctValues(Map<String, String> mapping,
                                                      List<String> columns,
                                                      Set<File> files,
                                                      Charset charset) {
        // Prepare
        String[] header = null;
        Set<List<String>> result = new HashSet<>();
        for (File file : files) {
            // Check header
            String[] header2 = getHeader(mapping, new HashSet<>(columns), file);
            if (header == null) {
                header = header2;
            } else if (!Arrays.equals(header, header2)) {
                LoggerUtil.fatalUnchecked("Aborting due to header mismatch in file" +
                                          file.getName(),
                                          CSVReader.class,
                                          RuntimeException.class);
            }
            result.addAll(getDistinctValues(mapping, columns, file, charset));
        }
        return result;
    }

    /**
     * Static method to read the header of a csv file.
     * 
     * @param mapping
     * @param fileName
     *            absolute file name
     * @return a string array
     */
    public static String[] getHeader(Map<String, String> mapping, File file) {
        return getHeader(mapping, file, null);
    }

    /**
     * Static method to read the header of a csv file.
     * 
     * @param mapping
     * @param fileName
     *            absolute file name
     * @param charset
     * @return a string array
     */
    public static String[] getHeader(Map<String, String> mapping, File file, Charset charset) {

        // Read the header
        String[] header = readFile(file, true, charset).get(0);

        // Check for empty file
        if (header.length <= 1) {
            LoggerUtil.fatalUnchecked("File is empty. Name of the empty file is " + file.getName(),
                                      CSVReader.class,
                                      RuntimeException.class);
        }
        return getMappedHeader(mapping, header);
    }

    /**
     * Static method to read the header of a set of csv files.
     * 
     * @param mapping
     * @param files
     *            a set of files
     * @return a string array
     */
    public static String[] getHeader(Map<String, String> mapping, Set<File> files) {
        return getHeader(mapping, files, null);
    }

    /**
     * Static method to read the header of a set of csv files.
     * 
     * @param mapping
     * @param files
     *            a set of files
     * @param charset
     * @return a string array
     */
    public static String[]
           getHeader(Map<String, String> mapping, Set<File> files, Charset charset) {
        // Prepare
        String[] header = getHeader(mapping, files.iterator().next(), charset);
        for (File file : files) {
            // Read the header
            String[] result = getHeader(mapping, file, charset);
            if (!Arrays.equals(result, header)) {
                LoggerUtil.fatalUnchecked("Aborting due to header mismatch in file" +
                                          file.getName(),
                                          CSVReader.class,
                                          RuntimeException.class);
            }
        }
        return header;
    }

    /**
     * Static method to read all rows of a csv file including the header are
     * read for each row.
     * 
     * @param mapping
     * @param columns
     *            the name of the columns to read
     * @param fileName
     *            absolute file name
     * @param charset
     * @return a List<String[]> object
     */
    public static List<String[]>
           getRows(Map<String, String> mapping, Set<String> columns, File file) {
        return getRows(mapping, columns, file, null);
    }

    /**
     * Static method to read all rows of a csv file including the header are
     * read for each row.
     * 
     * @param mapping
     * @param columns
     *            the name of the columns to read
     * @param fileName
     *            absolute file name
     * @param charset
     * @return a List<String[]> object
     */
    public static List<String[]>
           getRows(Map<String, String> mapping, Set<String> columns, File file, Charset charset) {

        // Read
        List<String[]> rows = readFile(file, false, charset);

        // Check for empty files
        if (rows.size() <= 1) {
            LoggerUtil.fatalUnchecked("File is empty. Name of the empty file is " + file.getName(),
                                      CSVReader.class,
                                      RuntimeException.class);
        }
        // Remove Duplicate Column Names in the header of the CSV file
        rows.set(0, getMappedHeader(mapping, rows.get(0)));
        List<Integer> indexesToReturn;

        // Initialize indexes to return. The indexes are used to perform
        // selections on the whole set read
        indexesToReturn = new ArrayList<>();
        for (String column : columns) {
            for (int i = 0; i < rows.get(0).length; i++) {
                if (rows.get(0)[i].equals(column)) {
                    indexesToReturn.add(i);
                }
            }
        }

        // Prepare
        List<String[]> result = new ArrayList<>();

        // From the whole set of data-sets read, select the required sub-set
        // using the column indexes initialized before (indexesToReturn)
        for (String[] row : rows) {
            String[] newRow = new String[indexesToReturn.size()];
            int indexCounter = 0;
            List<String> tmpRow = new ArrayList<String>(Arrays.asList(row));
            for (int i : indexesToReturn) {
                newRow[indexCounter++] = tmpRow.get(i);
            }
            result.add(newRow);
        }

        // Done
        return result;
    }

    /**
     * Static method to read all rows of a set of csv files including the header
     * are read for each file.
     * 
     * @param mapping
     * @param columns
     *            the name of the columns to read
     * @param set
     *            of absolute file names
     * @param charset
     * @return a List<String[]> object
     */
    public static List<String[]> getRows(Map<String, String> mapping,
                                         Set<String> columns,
                                         Set<File> files,
                                         Charset charset) {
        // Prepare
        List<String[]> result = new ArrayList<>();
        int fileCounter = 1;
        for (File file : files) {
            // Read
            List<String[]> rows = getRows(mapping, columns, file, charset);
            if (fileCounter == 1) {
                result.addAll(rows);
            } else if (fileCounter > 1 && Arrays.equals(result.get(0), rows.get(0))) {
                for (int i = 1; i < rows.size(); i++) {
                    result.add(rows.get(i));
                }
            } else {
                LoggerUtil.info("Previous: " + Arrays.toString(result.get(0)), CSVReader.class);
                LoggerUtil.info("Current: " + Arrays.toString(rows.get(0)), CSVReader.class);
                LoggerUtil.fatalUnchecked("Aborting due to header mismatch in file " +
                                          file.getName(),
                                          CSVReader.class,
                                          RuntimeException.class);
            }
            fileCounter++;
        }
        return result;
    }

    /**
     * Adds a file to the cache
     * 
     * @param file
     */
    public static void loadIntoCache(File file) {
        loadIntoCache(file, null);
    }

    /**
     * Adds a file to the cache
     * 
     * @param file
     * @param charset
     */
    public static void loadIntoCache(File file, Charset charset) {
        if (!CACHE.containsKey(file.getAbsolutePath())) {
            LoggerUtil.info("Caching file: " + file.getName(), CSVReader.class);
            CACHE.put(file.getAbsolutePath(), readFile(file, false, charset));
        }
    }

    /**
     * Adds a set of files to the cache
     * 
     * @param file
     */
    public static void loadIntoCache(Set<File> files) {
        loadIntoCache(files, null);
    }

    /**
     * Adds a set of files to the cache
     * 
     * @param file
     */
    public static void loadIntoCache(Set<File> files, Charset charset) {
        for (File file : files) {
            loadIntoCache(file, charset);
        }
    }

    /**
     * Returns the required charset
     * 
     * @param file
     * @param specified
     * @return
     */
    private static Charset getCharset(File file, Charset specified) {

        if (specified != null) {

            // Specified for this file
            return specified;

        } else if (Model.get().getConfig().getCharset() != null) {

            // Specified globally
            return Model.get().getConfig().getCharset();

        } else {

            // Detect
            String encoding;
            try {
                encoding = UniversalDetector.detectCharset(file);
                if (encoding != null) {
                    return Charset.forName(encoding);
                } else {
                    LoggerUtil.warn("Could not detect charset for: " + file.getName(),
                                    CSVReader.class);
                }
            } catch (IOException e) {
                LoggerUtil.warn("IO error when detecting charset for: " + file.getName(),
                                CSVReader.class);
            }
        }

        // Fall back to default
        return Charset.defaultCharset();
    }

    /**
     * Static method to read the header of a csv file.
     * 
     * @param mapping
     * @param columns
     * @param file
     * @return a string array
     */
    private static String[] getHeader(Map<String, String> mapping, Set<String> columns, File file) {

        // Read the header
        String[] header = getHeader(mapping, file);
        List<Integer> indexesToReturn = new ArrayList<>();
        for (String column : columns) {
            for (int i = 0; i < header.length; i++) {
                if (header[i].equals(column)) {
                    indexesToReturn.add(i);
                }
            }
        }
        String[] extractedHeader = new String[indexesToReturn.size()];
        int indexCounter = 0;
        List<String> tmpRow = new ArrayList<String>(Arrays.asList(header));
        for (int i : indexesToReturn) {
            extractedHeader[indexCounter++] = tmpRow.get(i);
        }
        return extractedHeader;
    }

    /**
     * Static method to detect duplicate columns names in the header of a CSV
     * file and rename the duplicate column names.
     * 
     * @param mapping
     * 
     * @param header
     *            header of a CSV file with column names to process
     * @return the processed header without duplicate column names
     * 
     */
    private static String[] getMappedHeader(Map<String, String> mapping, String[] header) {

        // Prepare
        String[] result = new String[header.length];

        // Example Input: [Patient,Fall,Fallart,Fallart]
        // Example Output: [Patient,Fall,Fallart,Fallart2]
        Map<String, Integer> counts = new HashMap<>();
        int index = 0;
        for (String column : header) {
            Integer count = counts.get(column);
            if (count == null) {
                counts.put(column, 1);
                result[index] = column;
            } else {
                count++;
                counts.put(column, count);
                result[index] = column + count;
            }
            index++;
        }

        // Always apply mapping
        if (mapping != null) {
            for (int i = 0; i < result.length; i++) {
                result[i] = mapping.getOrDefault(result[i], result[i]);
            }
        }

        // Done
        return result;
    }

    /**
     * Generically converts files, if needed
     * 
     * @param file
     * @return
     */
    private static File getReadableFile(File file) {

        // Currently, we only convert excel files
        if (!file.getName().toLowerCase().endsWith(".xlsx")) { return file; }

        // Create file, if needed
        String outputFileName = file.getAbsolutePath() + ".csv";
        final File outputFile = new File(outputFileName);
        if (outputFile.isFile()) { return outputFile; }

        // Delete file when shutting down the JVM
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                if (outputFile.exists()) {
                    try {
                        outputFile.delete();
                    } catch (Exception e) {
                        // Ignore
                    }
                }
            }
        });

        // Info
        LoggerUtil.info("Converting file: " + file.getName(), CSVReader.class);

        // Try to convert
        try {

            // Convert the first sheet
            int sheetIdx = 0;
            FileInputStream fileInStream = new FileInputStream(file);

            // Open the XLSX and get the requested sheet from the workbook
            XSSFWorkbook workBook = new XSSFWorkbook(fileInStream);
            XSSFSheet selSheet = workBook.getSheetAt(sheetIdx);
            Iterator<Row> rowIterator = selSheet.iterator();

            // Check
            if (!rowIterator.hasNext()) {
                LoggerUtil.fatalUnchecked("Empty file: " + file.getName(),
                                          CSVReader.class,
                                          IllegalStateException.class);
            }

            // Prepare for writing output
            String[] header = getRow(rowIterator.next());

            // Check
            if (header == null || header.length == 0) {
                LoggerUtil.fatalUnchecked("Empty file: " + file.getName(),
                                          CSVReader.class,
                                          IllegalStateException.class);
            }

            // Prepare writing
            CsvWriterSettings settings = new CsvWriterSettings();
            settings.setEmptyValue("");
            settings.setNullValue("");
            settings.setQuoteAllFields(false);
            settings.getFormat()
                    .setLineSeparator(ConfigurationFileFormat.getInstance().getNewLine());
            settings.getFormat()
                    .setDelimiter(ConfigurationFileFormat.getInstance().getDelimiterOutput());
            CsvWriter writer = new CsvWriter(outputFile, settings);
            writer.writeRow(header);

            // For each row
            while (rowIterator.hasNext()) {

                // Write row
                writer.writeRow(getRow(rowIterator.next()));
            }

            // Close in and out
            writer.close();
            workBook.close();

            // Catch errors
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }

        // Done
        return outputFile;
    }

    /**
     * Converts the row
     * 
     * @param next
     * @return
     */
    private static String[] getRow(Row row) {

        // Prepare
        Iterator<Cell> cellIterator = row.cellIterator();
        List<String> result = new ArrayList<>();

        // For each cell
        while (cellIterator.hasNext()) {

            // Get cell
            Cell cell = cellIterator.next();

            // Process cell, depending on type
            switch (cell.getCellTypeEnum()) {
            case STRING:
                result.add(cell.getStringCellValue());
                break;
            case NUMERIC:
                if (HSSFDateUtil.isCellDateFormatted(cell)) {
                    java.util.Date date = cell.getDateCellValue();
                    result.add(Timestamp.toInternalDate(date));
                } else {
                    result.add(ConfigurationInputFileNumberFormat.toInternalNumber(cell.getNumericCellValue()));
                }
                break;
            case BOOLEAN:
                result.add(String.valueOf(cell.getBooleanCellValue()));
                break;
            default:
                result.add("");
                break;
            }
        }

        // Done
        return result.toArray(new String[result.size()]);
    }

    /**
     * Returns whether this is a character that we keep in canonical form
     * 
     * @param c
     * @return
     */
    private static boolean isCanonicalCharacter(char c) {
        boolean asciiCharacter = (c > 32) && (c < 127);
        boolean greekCharacter = UnicodeScript.of(c) == UnicodeScript.GREEK;
        boolean unit = c == '\u00B5';
        return asciiCharacter || greekCharacter || unit;
    }

    /**
     * Returns whether this is a character that we can display without crashing
     * transmart
     * 
     * @param c
     * @return
     */
    private static boolean isDisplayableCharacter(char c) {
        boolean asciiCharacter = (c > 32) && (c < 127);
        boolean umlaut = c == 'ä' || c == 'ö' || c == 'ü' || c == 'ß';
        boolean unit = c == '\u00B5';
        boolean greekCharacter = UnicodeScript.of(c) == UnicodeScript.GREEK;
        return asciiCharacter || greekCharacter || umlaut || unit;
    }

    /**
     * Returns whether this is a basic ASCII number or letter
     * 
     * @param c
     * @return
     */
    private static boolean isLetterOrNumber(char c) {
        return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9');
    }

    /**
     * Adapted from
     * https://stackoverflow.com/questions/1437933/how-to-properly-trim-whitespaces-from-a-string-in-java
     * 
     * @param c
     * @return
     */
    private static boolean isWhitespace(char c) {
        return (c <= ' ') || (c == '\u00A0');
    }

    /**
     * Common method to read a CSV file
     * 
     * @param file
     * @param headerOnly
     * @param charset
     * @return
     */
    private static List<String[]> readFile(File file, boolean headerOnly, Charset charset) {

        // Make sure that we can read this file
        file = getReadableFile(file);

        // The settings for the parser
        CsvParserSettings settings = new CsvParserSettings();

        // Auto-detect: default is 1024
        settings.detectFormatAutomatically(DELIMITERS);
        settings.setMaxColumns(ConfigurationFileFormat.getInstance().getMaxcolsread());
        settings.setSkipEmptyLines(true);
        settings.setEmptyValue("");
        settings.setNullValue("");
        settings.trimValues(true);
        settings.trimQuotedValues(true);
        settings.setIgnoreLeadingWhitespaces(true);
        settings.setIgnoreTrailingWhitespaces(true);
        settings.setIgnoreTrailingWhitespacesInQuotes(true);
        settings.setIgnoreLeadingWhitespacesInQuotes(true);
        settings.setLineSeparatorDetectionEnabled(true);

        // The parser and result
        List<String[]> rows = CACHE.get(file.getAbsolutePath());

        // Header only
        if (headerOnly) {

            if (rows == null) {

                // Read file
                CsvParser parser = new CsvParser(settings);
                Charset _charset = getCharset(file, charset);
                parser.beginParsing(file, _charset);
                String[] header = parser.parseNext();
                parser.stopParsing();
                rows = new ArrayList<String[]>();
                rows.add(header);

                // Trim
                for (String[] row : rows) {
                    row = trim(row);
                }

            }

            // Complete file
        } else {

            if (rows == null) {

                // Read file
                CsvParser parser = new CsvParser(settings);
                Charset _charset = getCharset(file, charset);
                rows = parser.parseAll(file, _charset);
                parser.stopParsing();

                // Trim
                for (String[] row : rows) {
                    row = trim(row);
                }

            }
        }

        // Check
        if (rows.isEmpty()) {
            LoggerUtil.fatalUnchecked("File is empty: " + file.getName(),
                                      CSVReader.class,
                                      IllegalStateException.class);
        }

        // Done
        return rows;
    }

    /**
     * Trims various types of whitespaces
     * 
     * @param value
     * @return
     */
    private static String trim(String value) {

        int strLength = value.length();
        int len = value.length();
        int st = 0;
        char[] val = value.toCharArray();

        if (strLength == 0) { return ""; }

        while ((st < len) && isWhitespace(val[st])) {
            st++;
            if (st == strLength) {
                break;
            }
        }
        while ((st < len) && isWhitespace(val[len - 1])) {
            len--;
            if (len == 0) {
                break;
            }
        }

        return (st > len) ? "" : ((st > 0) || (len < strLength)) ? value.substring(st, len) : value;
    }

    /**
     * Trims all values in the array, *in place*
     * 
     * @param array
     * @return
     */
    private static String[] trim(String[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = trim(array[i]);
        }
        return array;
    }

    /**
     * Static method to get all rows of a csv file, including the header
     * 
     * @param mapping
     * @param columns
     *            the name of the columns to read
     * @param file
     *            absolute file name
     * @param filters
     *            the column filters
     * @param charset
     *            charset
     * @return a List<String[]> object
     */
    static List<String[]> getRows(Map<String, String> mapping,
                                  Set<String> columns,
                                  File file,
                                  ConfigurationInputFileColumnFilter[] filters,
                                  Charset charset) {

        // Check for filter. If no filter has been applied, read everything
        if (filters == null ||
            filters.length == 0) { return getRows(mapping, columns, file, charset); }

        // Prepare
        List<Integer> indexesToStage = new ArrayList<>();
        boolean extraColsPresent = false;
        Set<String> nColsToRead = new HashSet<>(columns);

        // Check if filter has been applied on a column which is not selected
        // for final staging If such a column is present, we add it here to read
        // for filtering purposes
        for (ConfigurationInputFileColumnFilter filter : filters) {
            for (String col : filter.getColumns()) {
                if (!columns.contains(col)) {
                    nColsToRead.add(col);
                    extraColsPresent = true;
                }
            }
        }

        // Read all rows using the newly calculated set of columns
        List<String[]> rows = CSVReader.getRows(mapping, nColsToRead, file, charset);

        // Check for empty files
        if (rows.size() <= 1) {
            LoggerUtil.fatalUnchecked("File is empty. Name of the empty file is " + file.getName(),
                                      CSVReader.class,
                                      RuntimeException.class);
        }

        // Initialize indexes to filter
        for (ConfigurationInputFileColumnFilter filter : filters) {
            filter.prepare(rows.get(0));
        }

        // Initialize indexes to return. The indexes are used to perform
        // selections on the whole set read
        indexesToStage = new ArrayList<>();
        for (String column : columns) {
            for (int i = 0; i < rows.get(0).length; i++) {
                if (rows.get(0)[i].equals(column)) {
                    indexesToStage.add(i);
                }
            }
        }
        // Prepare
        List<String[]> result = new ArrayList<>();

        // Add the header to the result set manually as the filtering loop below
        // skips it Every data-set from CSV files either full/partial should
        // always have the header as the first row
        if (!extraColsPresent) {
            result.add(rows.get(0));
        } else {
            String[] newRow = new String[indexesToStage.size()];
            int indexCounter = 0;
            List<String> tmpRow = new ArrayList<String>(Arrays.asList(rows.get(0)));
            for (int i : indexesToStage) {
                newRow[indexCounter++] = tmpRow.get(i);
            }
            result.add(newRow);
        }

        // Adds all rows in which the values of the attribute to filters equal
        // the configuration
        for (String[] row : rows) {

            // Check filters
            boolean passes = false;
            for (ConfigurationInputFileColumnFilter filter : filters) {
                if (filter.passes(row)) {
                    passes = true;
                    break;
                }
            }

            // From the whole set of data-sets read, select the required
            // sub-set using the column indexes initialized before stored in
            // (indexesToStage)
            if (passes) {
                String[] newRow = new String[indexesToStage.size()];
                int indexCounter = 0;
                List<String> tmpRow = new ArrayList<String>(Arrays.asList(row));
                for (int i : indexesToStage) {
                    newRow[indexCounter++] = tmpRow.get(i);
                }
                result.add(newRow);
            }
        }
        return result;
    }

    /**
     * Static method to get all rows of a set of csv files, including the header
     * 
     * @param mapping
     * @param columns
     *            the name of the columns to read
     * @param file
     *            a set of absolute file names
     * @param filter
     *            the column filter
     * @param charset
     * @return a List<String[]> object
     */
    static List<String[]> getRows(Map<String, String> mapping,
                                  Set<String> columns,
                                  Set<File> files,
                                  ConfigurationInputFileColumnFilter[] filters,
                                  Charset charset) {
        // Prepare
        List<String[]> result = new ArrayList<>();
        int fileCounter = 1;
        for (File file : files) {
            // Read
            List<String[]> rows = getRows(mapping, columns, file, filters, charset);
            if (fileCounter == 1) {
                result.addAll(rows);
            } else if (fileCounter > 1 && Arrays.equals(result.get(0), rows.get(0))) {
                for (int i = 1; i < rows.size(); i++) {
                    result.add(rows.get(i));
                }
            } else {
                LoggerUtil.info("Previous: " + Arrays.toString(result.get(0)), CSVReader.class);
                LoggerUtil.info("Current: " + Arrays.toString(rows.get(0)), CSVReader.class);
                LoggerUtil.fatalUnchecked("Aborting due to header mismatch in file " +
                                          file.getName(),
                                          CSVReader.class,
                                          RuntimeException.class);
            }
            fileCounter++;
        }
        return result;
    }
}
