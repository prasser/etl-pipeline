/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.util;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.univocity.parsers.csv.CsvFormat;

/**
 * Contains logging methods for component ETL
 * 
 * @author Tanmaya Mahapatra
 * @author Fabian Prasser
 */
public class LoggerUtil {

    /** Cache: maybe not needed */
    private static Map<Class<?>, Logger> cache = new HashMap<>();

    /**
     * Prints an error information and throws a checked exception
     * 
     * @param e
     *            exception
     * @param className
     *            name of the class from where the method is invoked
     * @param exceptionClass
     *            class name of checked exception to throw
     *
     */

    public static <T extends Exception> void fatalChecked(Exception e,
                                                          Class<?> clazz,
                                                          Class<T> exceptionClass) throws T {

        Logger logger = getLogger(clazz);
        logger.error(e.getMessage());
        throw getClassInstance(e, exceptionClass);
    }

    /**
     * Prints an error information and throws a checked exception
     * 
     * @param message
     *            to print
     * @param className
     *            name of the class from where the method is invoked
     * @param exceptionClass
     *            class name of checked exception to throw
     *
     */
    public static <T extends Exception> void fatalChecked(String message,
                                                          Class<?> clazz,
                                                          Class<T> exceptionClass) throws T {

        Logger logger = getLogger(clazz);
        logger.error(message);
        throw getClassInstance(message, exceptionClass);
    }

    /**
     * Prints an error information and throws an unchecked exception
     * 
     * @param message
     *            to print
     * @param className
     *            name of the class from where the method is invoked
     * @param exception
     *            name of unchecked exception to throw
     */

    public static <T extends RuntimeException> void fatalUnchecked(String message,
                                                                   Class<?> clazz,
                                                                   Class<T> exception) {

        Logger logger = getLogger(clazz);
        logger.error(message);
        throw getClassInstance(message, exception);
    }

    /**
     * Prints an error information and throws an unchecked exception
     * 
     * @param message
     *            to print
     * @param e
     *            exception
     * @param clazz
     *            name of the class from where the method is invoked
     * @param exception
     *            name of unchecked exception to throw
     */

    public static <T extends RuntimeException>
           void
           fatalUnchecked(String message, Exception e, Class<?> clazz, Class<T> exception) {

        Logger logger = getLogger(clazz);
        logger.error(message);
        throw getClassInstance(message, exception, e);
    }

    /**
     * Generically instantiate exception
     * 
     * @param wrappedException
     * @param exceptionClass
     * @return
     */
    public static <T extends Exception> T getClassInstance(Exception wrappedException,
                                                           Class<T> exceptionClass) {
        try {
            return exceptionClass.getConstructor(Exception.class).newInstance(wrappedException);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Generically instantiate exception
     * 
     * @param message
     * @param exception
     * @return
     */
    public static <T extends Exception> T getClassInstance(String message, Class<T> exception) {
        try {
            return exception.getConstructor(String.class).newInstance(message);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Generically instantiate exception
     * 
     * @param message
     * @param exception
     * @param wrappedException
     * @return
     */
    public static <T extends Exception> T getClassInstance(String message,
                                                           Class<T> exception,
                                                           Exception wrappedException) {
        try {
            return exception.getConstructor(String.class, Throwable.class)
                            .newInstance(message, wrappedException);
        } catch (Exception e) {

            throw new RuntimeException(e);
        }

    }

    /**
     * Log CSV format
     * 
     * @param f
     * @param clazz
     */
    public static void info(CsvFormat f, Class<?> clazz) {
        StringBuilder builder = new StringBuilder();
        builder.append("CSVFormat: Comment character=");
        builder.append(f.getComment());
        builder.append(", Field delimiter=");
        builder.append(f.getDelimiter());
        builder.append(", Line separator (normalized)=not shown");
        // builder.append(f.getNormalizedNewline());
        builder.append(", Line separator sequence=not shown");
        // builder.append(f.getLineSeparatorString());
        builder.append(", Quote character=");
        builder.append(f.getQuote());
        builder.append(", Quote escape character=");
        builder.append(f.getQuoteEscape());
        info(builder.toString(), clazz);
    }

    /**
     * Prints a logging information if value passed is greater than zero
     * 
     * @param value
     *            to check
     * @param message
     *            to print
     * @param className
     *            name of the class from where the method is invoked
     */
    public static void info(long value, String message, Class<?> clazz) {

        if (value > 0) {
            Logger logger = getLogger(clazz);
            logger.info(message);
        }
    }

    /**
     * Prints a logging information
     * 
     * @param message
     *            to print
     * @param className
     *            name of the class from where the method is invoked
     */
    public static void info(String message, Class<?> clazz) {

        Logger logger = getLogger(clazz);
        logger.info(message);
    }

    /**
     * Prints a warning information if value passed is greater than zero
     * 
     * @param value
     *            to check
     * @param message
     *            to print
     * @param className
     *            name of the class from where the method is invoked
     */
    public static void warn(int value, String message, Class<?> clazz) {

        if (value > 0) {
            Logger logger = getLogger(clazz);
            logger.warn(message);
        }
    }

    /**
     * Prints a warning information
     * 
     * @param message
     *            to print
     * @param className
     *            name of the class from where the method is invoked
     */
    public static void warn(String message, Class<?> clazz) {

        Logger logger = getLogger(clazz);
        logger.warn(message);
    }

    /**
     * Return logger
     * 
     * @param clazz
     * @return
     */
    private static Logger getLogger(Class<?> clazz) {
        Logger result = cache.get(clazz);
        if (result == null) {
            result = LoggerFactory.getLogger(clazz);
            cache.put(clazz, result);
        }
        return result;
    }
}
