/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import ch.qos.logback.core.spi.*

appender("LOGINFO", ConsoleAppender) {
  filter(ch.qos.logback.classic.filter.LevelFilter) {
    level = INFO;
    onMatch = FilterReply.ACCEPT;
    onMismatch = FilterReply.DENY
  }   
  encoder(PatternLayoutEncoder) {
    pattern = "%d [%thread] [%level] %logger{20} - %msg%n"
  }
}

appender("LOGWARN", ConsoleAppender) {
  filter(ch.qos.logback.classic.filter.LevelFilter) {
    level = WARN;
    onMatch = FilterReply.ACCEPT;
    onMismatch = FilterReply.DENY
  }   
  encoder(PatternLayoutEncoder) {
    pattern = "%d [%thread] [%level] %logger{20} - %msg%n"
  }
}

appender("LOGERROR", ConsoleAppender) {
  filter(ch.qos.logback.classic.filter.LevelFilter) {
    level = ERROR;
    onMatch = FilterReply.ACCEPT;
    onMismatch = FilterReply.DENY
  }   
  encoder(PatternLayoutEncoder) {
    pattern = "%d [%thread] [%level] %logger{20} - %msg%n"
  }
}

root(INFO, ["LOGINFO", "LOGWARN", "LOGERROR"])
